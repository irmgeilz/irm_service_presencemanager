package fr.IRM.PresenceManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IrmPresenceManagerMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrmPresenceManagerMsApplication.class, args);
	}

}
