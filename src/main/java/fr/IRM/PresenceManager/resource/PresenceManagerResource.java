package fr.IRM.PresenceManager.resource;

import java.io.IOException;

import obix.Obj;
import obix.io.ObixDecoder;

import org.eclipse.om2m.commons.mapper.Mapper;
import org.eclipse.om2m.commons.resource.ContentInstance;
import org.eclispe.om2m.commons.client.Client;
import org.eclispe.om2m.commons.client.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.IRM.PresenceManager.utils.PostUtils;

@RestController
public class PresenceManagerResource {
	
	private Mapper mapper = new Mapper();
	private static String baseUrl = "http://localhost:8080/~/";
	private static String originator = "admin:admin";
	private static String deviceName = "presence";
	
	//Function to retrieve the last state of the presence sensor in a specific room
	@GetMapping(value="/getPresenceState/{room}")
	public String getPresenceState(@PathVariable String room){
		String res = null;
		Client client = new Client();
		Response resp;		
		try {
			resp = client.retrieve(baseUrl + room + "/" + room + "/" + deviceName + "/data/la",originator);
			res = resp.getRepresentation();
			String s = resp.getRepresentation();
			ContentInstance cin = (ContentInstance)mapper.unmarshal(s);
			Obj o = ObixDecoder.fromString(cin.getContent());
			res = Integer.toString((int)o.get("Value").getInt());
		} catch(IOException e) {
			e.printStackTrace();
		}

		return res;
	}

	//This service allow the customer to "set" the presence sensor state of the room (by posting a 1 boolean in the cin)
	@PostMapping(value="/setPresence/{room}")
	public String setPresence(@PathVariable String room){
		String res = null;
		Client client = new Client();
		Response resp;
		try {
			resp = PostUtils.createCIN(PostUtils.generateStateCIN(room, this.deviceName, 1), baseUrl + room + "/" + room + "/" + deviceName + "/data", originator, client);
			res = resp.getRepresentation();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return res;
	}

	//This service allow the customer to "reset" the presence sensor state of the room (by posting a 0 boolean in the cin)
	@PostMapping(value="/resetPresence/{room}")
	public String resetPresence(@PathVariable String room){
		String res = null;
		Client client = new Client();
		Response resp;
		try {
			resp = PostUtils.createCIN(PostUtils.generateStateCIN(room, deviceName, 0), baseUrl + room + "/" + room + "/" + deviceName + "/data", originator, client);
			res = resp.getRepresentation();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return res;
	}
}
